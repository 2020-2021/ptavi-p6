#!/usr/bin/python3.
# -*- coding: utf-8 -*-.

import socket
import sys

try:
    methodo = str(sys.argv[1])
    recibe = str(sys.argv[2])
except ValueError:
    sys.exit(" SIP/2.0 400 Bad Request ")
except IndexError:
    sys.exit(" Usage: python3 client.py methodo recibe@IP:SIPport ")

recibid1 = recibe.split(":")
recibid2 = recibid1[0].split("@")


if methodo == "INVITE":
    LINE = (methodo + " sip:" + recibe[0:20] + " SIP/2.0\r\n")

if methodo == "BYE":
    LINE = (methodo + " sip:" + recibe[0:20] + " SIP/2.0\r\n")


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    ADDR = recibid2[1]
    LOGN = recibid2[0]
    PORT = recibid1[1]
    SERV = "127.0.0.1"
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERV, int(PORT)))

    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))
    if methodo == "INVITE":
        intento = "SIP/2.0 100 TRYING"
        llamada = "SIP/2.0 180 RING"
        recibo = "SIP/2.0 200 OK"
        transformo = data.decode('utf-8').split("\r\n\r\n")

        if transformo[2] == "SIP/2.0 200 OK":
            methodo2 = "ACK"
            LINE1 = (methodo2 + " sip:" + recibe + "\r\n" + " SIP/2.0\r\n")
            my_socket.send(bytes(LINE1, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)

print("Fin.")
