
import socketserver
import simplertp
import secrets
import sys


class EchoHandler(socketserver.DatagramRequestHandler):
    def handle(self):
        self.wfile.write(b"Recibida la peticion")
        while 1:
            line = self.rfile.read()

            if not line:
                break
            chang = line.decode('utf-8')
            changing = chang.split(" ")
            if changing != "\r\n":
                receiver1 = changing[1].split("@")[1]
                usern = changing[1].split("@")[0].split(":")[1]
                port = changing[1].split("@")[1].split(":")[0]
                c = "c = 0\r\n"
                i = 'I = ' + str(usern) + ' ' + str(receiver1) + '\r\n'
                a = 'a = sessionm\r\n'
                r = 'r = 0\r\n'
                n = 'n = audio ' + str(port) + ' RTP\r\n'
                all = c + a + r + i + n

            if changing == 'sip':
                self.wfile.write(b" SIP/2.0 400 Bad Request\r\n\r\n")

            if changing[0] == "INVITE":

                try:
                    lin = line.decode('utf-8')
                    o = lin.split(':')[1].split('@')[0]

                    pp = 'SIP/2.0 100 Trying\r\n\r\n'
                    pp += 'SIP/2.0 180 Ringing\r\n\r\n'
                    pp += ' SIP/2.0 200 OK\r\n\r\n'

                    SDP_ = '\r\n\r\nv=0\r\n' + 'o=' + o + '@' + \
                           '127.0.0.1\r\n' + 's= emiliorrsession\r\n'
                    SDP_ += 't=0\r\n' + 'm=audio 67876 RTP\r\n\r\n'

                    CT = 'Content-Type: application/sdp'

                    self.wfile.write(bytes(pp + CT + SDP_, 'utf-8') + b'\r\n')
                except IndexError:
                    self.wfile.write(b'SIP/2.0 400 Bad Requestr\n')

            elif changing[0] == "BYE":
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif changing[0] == 'ACK':
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT, payload_type=14,
                                      ssrc=200002)
                audio = simplertp.RtpPayloadMp3(ARCHIVE)
                ip = '127.0.0.1'
                puerto = 23032
                simplertp.send_rtp_packet(RTP_header, audio, ip, puerto)
            elif changing != ("INVITE", "BYE", "ACK"):
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    try:
        InPr = sys.argv[1]
        PUERT = sys.argv[2]
        ARCHIVE = sys.argv[3]

    except IndexError:
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer((InPr, int(PUERT)), EchoHandler)
    serv.serve_forever()
